/*!
 * Digits v1.0
 * (c) Sadir Babayev
 * Released under the MIT Licence
 */
class Digits {
    constructor(number) {
        this.number = number + "";
        this._decimals = this.number.split(".")[1];
        // for optimization purposes
        this._decimalsLength = this._decimals.length;
        this._cachedValues = {};
    }

    static async getNumber(file) {
        const res = await fetch(file);
        return await res.text();
    }

    getDistribution(...breakpoints) {
        this.breakpoints = breakpoints;
        let distributions = {};
        let emptyDistribution = Array(10).fill(0);

        if (!breakpoints.length) {
            return this._determineDistribution(emptyDistribution, 0, this._decimalsLength);
        }

        this._checkBreakpoints();
        breakpoints.forEach((breakpoint, i) => {
            let distribution = i > 0 ? distributions[this.breakpoints[i - 1]] : emptyDistribution;
            let start = i > 0 ? breakpoints[i - 1] : 0;
            distributions[breakpoint] = this._determineDistribution(distribution, start, breakpoint);
        });

        return breakpoints.length > 1 ? distributions : distributions[breakpoints[0]];
    }

    _checkBreakpoints() {
        this.breakpoints.forEach((breakpoint, i) => {
            if (breakpoint > this._decimalsLength) {
                throw new RangeError("Breakpoint cannot be larger than the number of decimals.");
            }

            if (i > 0 && breakpoint < this.breakpoints[i - 1]) {
                throw new Error("Breakpoint must me larger than preceding breakpoint.");
            }
        });
    }

    _determineDistribution(distribution, start, breakpoint) {
        if (this._cachedValues[breakpoint]) {
            return this._cachedValues[breakpoint];
        }

        distribution = [...distribution];

        for (let i = start; i < breakpoint; i++) {
            distribution[this._decimals[i]]++;
        }

        this._cachedValues[breakpoint] = distribution;
        return distribution;
    }
}